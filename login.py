#
#  MIT License
#
#  Copyright (c) 2019 Proof Trading, Inc.
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#

import datetime
import json
import logging
import os

import firebase_admin
import flask
from firebase_admin import auth
from flask import request
from werkzeug.exceptions import Unauthorized

from util import measure_time

firebase_app = firebase_admin.initialize_app()

TOKEN_NAME = 'id_token'
COOKIE_NAME = 'id_token'
COOKIE_EXPIRES_IN = datetime.timedelta(days=14)

LOG = logging.getLogger(__name__)


@measure_time
def init_session():
    """
    Parses the auth token from request body json, turns it into a cookie, and sticks it into the response
    :return: The actual response that should be sent back
    """
    # see if we can retrieve and validate the auth cookie
    try:
        claims, session_cookie = check_token_and_create_cookie()
        process_claims(claims)
    except Exception:
        LOG.exception('Error checking token or creating cookie')
        raise

    # create response and set cookie
    expires = datetime.datetime.now() + COOKIE_EXPIRES_IN
    response = flask.jsonify({'status': 'logged in'})
    response.set_cookie(COOKIE_NAME, session_cookie,
                        expires=expires, httponly=True, secure=use_secure_cookie())
    return response


@measure_time
def check_request_auth():
    """
    Checks for the auth cookie to make sure it is present and valid
    :return: None
    """
    try:
        claims = check_id_token_cookie()
        process_claims(claims)
    except Exception:
        LOG.exception('Error checking id_token cookie')
        raise


def process_claims(claims):
    name = claims.get('name', 'Unknown')
    email = claims.get('email', 'Unknown')
    request.environ['REMOTE_USER'] = email
    LOG.info(f'Valid user authenticated. Name={name}, Email={email}')


def is_dev_admin():
    dev_admins = json.loads(os.environ.get('GAE_DEV_ADMIN', '[]'))
    return request.remote_user in dev_admins


@measure_time
def end_session():
    """
    Clears the auth cookie
    :return: The actual response that should be sent back
    """
    response = flask.jsonify({'status': 'logged out'})
    response.delete_cookie(COOKIE_NAME)
    return response


def check_token_and_create_cookie():
    """
    This function pulls out 'id_token' from the request body json, validates it, and creates a
    session cookie that can be stored with the browser.  This session cookie can later be validated
    without making a server call to google/firebase (assuming google's public certs are cached).

    :return: Tuple (decoded claims, session_cookie, expires_in)
    """
    request_data = request.get_json(force=True)
    if not request_data:
        raise Unauthorized('Request body could not be read as json')
    id_token = request_data.get(TOKEN_NAME)
    if not id_token:
        raise Unauthorized('No id_token found')

    try:
        claims = auth.verify_id_token(id_token, firebase_app)
    except ValueError:
        raise Unauthorized('Invalid ID Token')
    except auth.AuthError:
        raise Unauthorized('Auth failure')

    try:
        # Create the session cookie. The session cookie will have the same claims as the ID token.
        session_cookie = auth.create_session_cookie(id_token, expires_in=COOKIE_EXPIRES_IN)
        return claims, session_cookie
    except ValueError:
        raise Unauthorized('Invalid ID Token')
    except auth.AuthError:
        raise Unauthorized('Failed to create auth cookie')


def check_id_token_cookie():
    """
    This function tries to find a cookie called 'id_token' in the request and try to validate it
    :return: decoded claims
    """
    id_token_cookie = request.cookies.get(COOKIE_NAME)
    if not id_token_cookie:
        raise Unauthorized('No auth cookie found')

    try:
        LOG.info('Checking if auth cookie is valid...')
        claims = auth.verify_session_cookie(id_token_cookie)
        return claims
    except ValueError:
        raise Unauthorized('Invalid Auth Cookie')
    except auth.AuthError:
        raise Unauthorized('Auth failure on cookie')


def use_secure_cookie():
    return not flask.current_app.debug
