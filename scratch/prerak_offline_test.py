#                                                                               
#  MIT License                                                                  
#                                                                               
#  Copyright (c) 2019 Proof Trading, Inc.                                       
#                                                                               
#  Permission is hereby granted, free of charge, to any person obtaining a copy 
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights 
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
#  copies of the Software, and to permit persons to whom the Software is        
#  furnished to do so, subject to the following conditions:                     
#                                                                               
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.                              
#                                                                               
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.                                                                    
#                                                                               

import os
import time

from pandas.util.testing import assert_frame_equal

import logging_helper
from marketdata.polygon import PolygonDataSource, unmap_json_fields

logging_helper.init_logging()

test_cache_dir = os.path.normpath('../tests/test_data')
print(test_cache_dir)
ds = PolygonDataSource(cache_dir=test_cache_dir)

t1 = time.time()
trades = ds.load_json('trades_v2.json')
trades = unmap_json_fields(trades, ds.format_trade_v2)
# bars1 = ds.calc_bars(trades, groupby_freq='1s', time_field='participant_timestamp')
bars2 = ds.calc_bars(trades, groupby_freq='s', time_field='participant_timestamp')
print(bars2)
# bars2 = bars2[['participant_timestamp', 'n', 'open', 'close', 'high', 'low', 'volume']]

# for i in range(865):
#     open1 = bars1.iloc[i]['open']
#     open2 = bars2.iloc[i]['open']
#     if open1 != open2:
#         print(bars1.iloc[i], bars2.iloc[i])
#
# assert_frame_equal(bars1, bars2)
# print('equal')
