#                                                                               
#  MIT License                                                                  
#                                                                               
#  Copyright (c) 2019 Proof Trading, Inc.                                       
#                                                                               
#  Permission is hereby granted, free of charge, to any person obtaining a copy 
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights 
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
#  copies of the Software, and to permit persons to whom the Software is        
#  furnished to do so, subject to the following conditions:                     
#                                                                               
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.                              
#                                                                               
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.                                                                    
#                                                                               

import json
import logging
import os
from datetime import datetime

import pytz

import logging_helper

from marketdata.mds import MarketDataService
from util import MeasureTime

os.environ['DEFAULT_CACHE_IMPL'] = 'postgres'

logging_helper.init_logging()
logger = logging.getLogger('prerak_test')

tz = pytz.timezone('US/Eastern')
start = tz.localize(datetime(2019, 7, 1, 9, 50, 0, 0))
end = tz.localize(datetime(2019, 7, 1, 10, 55, 0, 9))
end_short = tz.localize(datetime(2019, 7, 1, 9, 55, 00, 0))

# print(start)
# print(start.replace(second=0, microsecond=0))
# print(start.replace(second=0, microsecond=0, minute=start.minute+1))
# print(start.timestamp())
# print(datetime.fromtimestamp(start.timestamp()))

mds = MarketDataService()

# data = mds.get_minute_bars('IBM', start, end)
# print(json.dumps(data, indent=4))
#
with MeasureTime('test', logging.getLogger('test')) as m:
    data = mds.get_full_ticks('AAPL', start, end_short)
# print(json.dumps(data, indent=4))

# data = mds.get_second_bars('AAPL', start, end_short)
# print(f'duration: {time.time() - t1}')
# print(json.dumps(data, indent=4))

print(len(json.dumps(data, sort_keys=False)), data['start_str'], data['end_str'])
