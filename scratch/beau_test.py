#                                                                               
#  MIT License                                                                  
#                                                                               
#  Copyright (c) 2019 Proof Trading, Inc.                                       
#                                                                               
#  Permission is hereby granted, free of charge, to any person obtaining a copy 
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights 
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
#  copies of the Software, and to permit persons to whom the Software is        
#  furnished to do so, subject to the following conditions:                     
#                                                                               
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.                              
#                                                                               
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.                                                                    
#                                                                               

import datetime
import json
import logging

import pandas as pd

from logging_helper import init_logging
from marketdata.polygon import PolygonDataSource, unmap_json_fields
from util import translate_to_datetime64, del_fields

init_logging()
LOG = logging.getLogger("GO")
LOG.info("start")

ds = PolygonDataSource(cache_dir="C:/dev/data/pg_cache")
dt = datetime.datetime(2019, 6, 14)


def get_test_trade_data():
    formatted = False
    for sym in ['IBKR', 'XLB', 'FOX', 'IBM', 'LOV', 'CBOE']:
        trades = ds.get_trades(sym, dt, 50000, formatted)
        LOG.info("%s - %d", sym, len(trades['ticks']))
        ds.save_json('trades_fullday_%s.json' % sym, trades)


def get_test_quote_data():
    formatted = False
    for sym in ['IBKR', 'XLB', 'FOX', 'IBM', 'LOV', 'CBOE']:
        data = ds.get_quotes(sym, dt, 50000, formatted)
        LOG.info("%s - %d", sym, len(data['ticks']))
        ds.save_json('quotes_%s.json' % sym, data)


def get_test_bars(syms, time_interval, time_unit):
    for sym in syms:
        bars = ds.get_bars(sym, dt, dt + datetime.timedelta(days=1), time_interval, time_unit, False)
        LOG.info("\n%s" % json.dumps(bars, indent=4))
        ds.save_json('bars_%s_%s_%s.json' % (time_interval, time_unit.name, sym), bars)


def get_test_nbbo(syms):
    for sym in syms:
        nbbo = ds.get_nbbo(sym, datetime.datetime(2015, 11, 5, 9, 35, 0), None, 10, False)
        LOG.info("\n%s" % json.dumps(nbbo, indent=4))
        # ds.save_json('nbbo.json', nbbo)


def get_trades_from_v2():
    dt_ticks = datetime.datetime(2015, 11, 5, 9, 35, 0)
    rt2 = ds.get_trades_v2('IBKR', dt_ticks, None, 10, False)
    ds.save_json('trades_v2.json', rt2)


def check_oddlot_conditions(self):
    ds2 = PolygonDataSource(cache_dir="C:/temp/pg")

    dt_start = datetime.datetime.strptime('2019-07-09 09:45:00', '%Y-%m-%d %H:%M:%S')
    dt_end = dt_start + datetime.timedelta(minutes=345)
    raw_data = ds2.get_trades_v2('BRK.A', dt_start, dt_end, 1000, False)
    ds2.save_json("ibkr_trades.json", raw_data)
    trades = unmap_json_fields(raw_data, ds2.format_trade_v2)
    translate_to_datetime64(trades, 'sip_timestamp')
    del_fields(trades, 'correction', 'id', 'orig_id', 'tape', 'trf_id', 'trf_timestamp')
    df_trades = pd.DataFrame(trades)
    LOG.info("\n%s", df_trades[(df_trades['size'] >= 100) & df_trades['conditions'].apply(lambda x: x and 37 in x)])
    LOG.info("\n%s", df_trades)


# get_test_quotes_from_ticks(['IBKR'])
# get_test_nbbo(['IBKR'])
# get_test_bars(['IBKR', 'LOV', 'CBOE', 'FOX', 'IBM'], 1, TimeUnit.minute)

# quotes = ds.get_quotes('IBKR', datetime.datetime(2019, 6, 14, 10, 0, 0), 50, True)
# ds.save_json('trades_SPY.json', trades)

# get_test_quote_data()
# last_ts = None
# for trade in trades if formatted else trades['ticks']:
#     log.info(trade)
#     last_ts = trade['timestamp']

# trades = ds.get_trades('IBKR', last_ts, 10, formatted)
# for trade in trades if formatted else trades['ticks']:
#     log.info(trade)

# if not formatted:  ds.save_json('trades.json', trades)


# trade conditions
# print(get_trade_cond_map())

LOG.info("done")
