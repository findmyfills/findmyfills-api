#
#  MIT License
#
#  Copyright (c) 2019 Proof Trading, Inc.
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#

import json
import os
import re
import urllib.parse
from datetime import datetime, timedelta

import flask
from flask import Flask, send_from_directory
from flask_cors import CORS
from werkzeug.exceptions import BadRequest, HTTPException

from analytics.fill_data import FillDataParser, FillDataAnalyzer
from logging_helper import enhance_logger, log_startup
from login import init_session, check_request_auth, end_session, is_dev_admin
from marketdata.mds import MarketDataService
from util import millis_to_datetime, measure_time, is_dev_service

APPROVED_ORIGINS = json.loads(os.environ['CORS_APPROVED_ORIGINS'])
LOGIN_EXEMPT_PATHS = json.loads(os.environ['LOGIN_EXEMPT_PATHS'])

LOGIN_EXEMPT_PATTERNS = [re.compile(p) for p in LOGIN_EXEMPT_PATHS]

app = Flask(__name__)
CORS(app, origins=APPROVED_ORIGINS, supports_credentials=True)

# Market Data Service - created in app_init()
mds: MarketDataService


def do_startup(debug=False):
    enhance_logger(app, debug)
    log_startup(app)
    app_init()


def app_init():
    # don't sort keys in returned json responses (Flask does this in hopes of making responses more cacheable)
    app.config['JSON_SORT_KEYS'] = False

    global mds
    mds = MarketDataService()


@app.errorhandler(Exception)
def bad_request(error):
    code = 500
    message = "Internal Server Error"
    if isinstance(error, HTTPException):
        code = error.code
        message = str(error)
        app.logger.warning(f"Request error: {message}")
    else:
        app.logger.exception("Unexpected error")
    return flask.jsonify(error=message), code


@app.before_request
def before_request():
    flask.request.proof_start_time = datetime.now()
    app.logger.info(f'Request received. Headers: {dict(flask.request.headers)}')

    if flask.request.method in ['GET', 'POST', 'PUT', 'DELETE']:
        # check that appropriate credentials were sent down
        exempt = False
        for p in LOGIN_EXEMPT_PATTERNS:
            if p.fullmatch(flask.request.path):
                exempt = True
                break
        if not exempt:
            check_request_auth()


@app.after_request
def after_request(response):
    # https://github.com/OWASP/CheatSheetSeries/blob/master/cheatsheets/HTTP_Strict_Transport_Security_Cheat_Sheet.md
    response.headers['Strict-Transport-Security'] = 'max-age=31536000; includeSubDomains'
    # track request time
    if hasattr(flask.request, 'proof_start_time'):
        # noinspection PyUnresolvedReferences,PyTypeChecker
        duration = datetime.now() - flask.request.proof_start_time
        app.logger.info(f'Request duration: {duration}')
    return response


@app.route('/_ah/warmup')
@measure_time
def handle_warmup_request():
    # do any best efforts warmup, such as initializing MD cache
    from marketdata.cache.mds_cache import MDSCacheProvider
    MDSCacheProvider()
    return 'done', 200, {}


@app.route('/', methods=['GET'])
def handle_root():
    if not is_dev_service() or not is_dev_admin():
        return 'up', 200, {}
    data = {'current_time': datetime.now(),
            'app.config': app.config, 'globals': globals(), 'os.environ': dict(os.environ),
            'request.environ': dict(flask.request.environ), 'request.headers': dict(flask.request.headers)}
    resp_string = json.dumps(data, indent=2, separators=(', ', ': '), default=str)
    resp = flask.make_response(resp_string, 200, {'Content-Type': 'application/json'})
    return resp


# The login page is only for dev testing
@app.route('/login/<path:path>', methods=['GET'])
def handle_login_files(path):
    if not is_dev_service():
        raise BadRequest('Unsupported Request')
    return send_from_directory('login', path)


@app.route('/chart/<granularity>/<sym>/<int:start_time>/<int:end_time>/',
           strict_slashes=False, methods=['GET'])
def get_stock_chart(sym, granularity, start_time, end_time):
    """
    Gets a stock chart. This can be an OHLC or Trades + Quotes

    Validates:
    1) symbol - no longer than 12 characters
    2) start / end times - expected ms since epoch, start < end, should be in 2019, and fall on the same day
    """
    if len(sym) > 12:
        raise BadRequest(f'Invalid symbol: {urllib.parse.quote(sym)}')

    try:
        requested_start = millis_to_datetime(start_time)
    except Exception as e:
        raise BadRequest(f'Invalid start time: {start_time}') from e

    try:
        requested_end = millis_to_datetime(end_time)
    except Exception as e:
        raise BadRequest(f'Invalid end time: {end_time}') from e

    if requested_start >= requested_end:
        raise BadRequest(f'Bad sequence of start/end times')

    if requested_start.date() != requested_end.date():
        raise BadRequest(f'Start and End times must fall on the same date')

    if requested_start.year < 2019 or requested_end.year < 2019:
        raise BadRequest('Unsupported dates')

    args = (sym, requested_start, requested_end)
    kwargs = get_chart_known_args()

    if granularity == 'min':
        resp_data = mds.get_minute_bars(*args, **kwargs)
    elif granularity == 'sec':
        resp_data = mds.get_second_bars(*args, **kwargs)
    elif granularity == 'full':
        # give the client some extra data on both sides for smoother charting
        requested_start -= timedelta(seconds=10)
        requested_end += timedelta(seconds=10)
        resp_data = mds.get_full_ticks(sym, requested_start, requested_end, **kwargs)
    else:
        raise BadRequest('Unsupported granularity')

    return flask.jsonify(resp_data), 200


def get_chart_known_args():
    args = flask.request.args
    kwargs = {}

    cached = args.get('cached', 'true')
    cached = cached.lower() not in ['false', 'n', '0']
    kwargs['cached'] = cached

    return kwargs


@app.route('/csv/', strict_slashes=False, methods=['POST'])
def get_fill_analytics():
    """
    Expects a POST request with fills in the body of the request. Fills are expected to be
    formatted as a list of rows, each of which is represented as a list of cells.

    The request body is expected to be JSON of this form:
        {
            'csv': [[h1, h2, h3, ...], [r1c1, r1c2, r1c3, ...], [r2c1, r2c2, r3c3, ...], ...],
            'symbol': sym,
            'side': 'BUY|SELL',
            'date': YYYY-MM-DD,
            'tz': 'ET|UTC'
        }

    :return: Analytics about these fills
    """

    app.logger.debug(f'POST request received with data: {flask.request.data}')

    fill_data = flask.request.get_json(force=True)
    if not fill_data:
        raise BadRequest('Invalid POST body')

    symbol = fill_data.get('symbol', None)
    if not symbol:
        # raise BadRequest('Could not determine symbol')
        app.logger.warning('CSV POST request received without symbol, will try to infer')

    side = fill_data.get('side', None)
    if not side or side not in ['BUY', 'SELL']:
        # raise BadRequest('Could not determine side')
        app.logger.warning('CSV POST request received without side, will try to infer')
        side = None

    date = fill_data.get('date', None)
    if not date:
        # raise BadRequest('Could not determine date for fills')
        app.logger.warning('CSV POST request received without date, will try to infer')
    if date == 'MM-DD-YYYY':
        date = None

    tz = fill_data.get('tz', None)
    if not tz or tz not in ['ET', 'UTC']:
        # raise BadRequest('Could not determine timezone for fills')
        app.logger.warning('CSV POST request received without tz, defaulting to ET')
        tz = 'ET'

    data = fill_data.get('csv', None)
    if not data:
        raise BadRequest('Could not read fill information')

    fdp = FillDataParser(symbol, side, date, tz, data)
    df = fdp.process()

    fda = FillDataAnalyzer(df, mds.ds)
    resp = fda.analyze()

    return flask.jsonify(resp), 200


@app.route('/initSession', strict_slashes=False, methods=['POST'])
def init_authenticated_session():
    return init_session()


@app.route('/endSession', strict_slashes=False, methods=['POST'])
def end_authenticated_session():
    return end_session()

# https://jsfiddle.net/preraksanghvi/n10wbu3v/
