#                                                                               
#  MIT License                                                                  
#                                                                               
#  Copyright (c) 2019 Proof Trading, Inc.                                       
#                                                                               
#  Permission is hereby granted, free of charge, to any person obtaining a copy 
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights 
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
#  copies of the Software, and to permit persons to whom the Software is        
#  furnished to do so, subject to the following conditions:                     
#                                                                               
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.                              
#                                                                               
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.                                                                    
#                                                                               

import datetime
import logging

from marketdata.cache.mds_cache import mds_cached
from marketdata.mds_util import generate_response, filter_ticks
from util import datetime_to_millis, ceil_dt, floor_dt
from .polygon import PolygonDataSource

LOG = logging.getLogger(__file__)


class MarketDataService:
    def __init__(self, ds=None):
        self.log = logging.getLogger(__name__)
        self.ds = ds if ds else PolygonDataSource()

    @mds_cached(enabled=False)
    def get_minute_bars(self, sym, start, end):
        """
        Gets minute bars from polygon for the requested symbol between start and end times. If start and end times
        do not fall on a minute boundary, the range will be expanded to make them move to the minute boundary.

        :param sym: Requested symbol
        :param start: start time as a datetime obj
        :param end: end time as a datetime obj
        :return:
        - Some values such as symbol, and rounded start/end times are returned as key value pairs in a dictionary
        - The actual ticks are returned as a list of lists, where each point is represented as
            [timestamp, open, high, low, close, vol]
        - The intention is to make the return format as natively aligned to HighCharts as possible, while keeping
        the data fairly compact on the wire
        - The returned ticks are filtered using: (rounded_start <= timestamp < rounded_end)

        Example:
        {sym: 'sym', start: startmillis, end: endmillis, ticks: [[tsmillis, o, h, l, c, v], [tsmillis, o, h, l, c, v]]}
        """

        # rounded start/end times
        r_start = floor_dt(start, datetime.timedelta(minutes=1))
        r_end = ceil_dt(end, datetime.timedelta(minutes=1))

        # query start/end times
        q_start = r_start
        # coz Polygon requires a minimum end time of T+1 - maybe move this to polygon.py
        q_end = r_start + datetime.timedelta(days=1)

        data = self.ds.get_bars(sym, q_start, q_end, convert=False)
        ticks = [[point["t"], point["o"], point["h"], point["l"], point["c"], point["v"]] for point in data["results"]]

        ticks = filter_ticks(ticks, datetime_to_millis(r_start), datetime_to_millis(r_end))
        result = generate_response(sym, r_start, r_end, "ohlc", "min", ticks)
        return result

    @mds_cached
    def get_second_bars(self, sym, start, end):
        """
        Similar to minute bars, but synthesized second bars

        We think of bars as ticks happening at specific points in time - in the case of second bars, they happen at the
        start of a given second. This means that if our start time falls in the middle of a second, the bar for that
        second should be excluded from the output. It also means that if our end time falls in the middle of a second,
        the bar for that second should be included in the output.

        This is why we would *like* to `ceil` both start and end times when getting the raw trades. This would ensure
        that only the relevant bars are created and included in the output.  *However*, if we `ceil` the start time,
        we are technically violating the contract and not providing the response for part of the requested range.

        Hence, we round down on start time and round up on end time.
        """

        # rounded start/end times
        r_start = floor_dt(start, datetime.timedelta(seconds=1))
        r_end = ceil_dt(end, datetime.timedelta(seconds=1))

        trades = self.ds.get_trades_v2(sym, r_start, r_end, limit=self.ds.NO_QUERY_LIMIT, convert=True)
        if trades:
            bars = self.ds.calc_bars(trades, groupby_freq='1s', time_field='participant_timestamp')
            ticks = [[datetime_to_millis(cells[0]), *cells[1:]] for cells in bars.itertuples(index=False)]
        else:
            ticks = []

        result = generate_response(sym, r_start, r_end, "ohlc", "sec", ticks)
        return result

    @mds_cached
    def get_full_ticks(self, sym, start, end):
        """
        Gets Last-Sale-Eligible trades and NBBO from polygon for the requested symbol between start and end times.

        :param sym: Requested symbol
        :param start: start time as a datetime obj
        :param end: end time as a datetime obj
        :return:
        - Some values such as symbol, and rounded start/end times are returned as key value pairs in a dictionary
        - The actual trades are returned as a list of lists, where each point is represented as
            [timestamp, px, size]
        - The intention is to make the return format as natively aligned to HighCharts as possible, while keeping
        the data fairly compact on the wire
        - The returned ticks are filtered using: (rounded_start <= timestamp < rounded_end)

        Example:
        {sym: 'sym', start: startmillis, end: endmillis, ticks: [[tsmillis, px, sz], [tsmillis, px, sz]]}
        """

        # get trades, and send only LSE trades
        data = self.ds.get_trades_v2(sym, start, end, limit=self.ds.NO_QUERY_LIMIT, convert=True)
        ticks = [[tick['participant_timestamp'], tick['price'], tick['size'], tick['flags']]
                 for tick in data if 'c' in tick['flags']]

        # get NBBO
        data = self.ds.get_nbbo(sym, start, end, limit=self.ds.NO_QUERY_LIMIT, convert=True)
        nbbo = [[tick['participant_timestamp'],
                 tick['bid_price'], tick['ask_price'],
                 tick['bid_size'], tick['ask_size'],
                 tick['bid_exchange'], tick['ask_exchange']] for tick in data]

        result = generate_response(sym, start, end, "ticks", "full", ticks, nbbo)
        return result

# TRADE_FIELDS = ['participant_timestamp', 'price', 'size', 'conditions']

# def format_items(raw_results,
#                  pre_filter_func=lambda x: x,
#                  format_func=lambda x: x,
#                  post_filter_func=lambda x, y: y):
#     results = []
#     for result in raw_results:
#         result = pre_filter_func(result)
#         if result:
#             formatted_result = format_func(result)
#             if formatted_result:
#                 formatted_result = post_filter_func(result, formatted_result)
#                 if formatted_result:
#                     results.append(formatted_result)
#     return results

# def trades_pre_filter_func(condition_field, trade):
#
#
# def trades_format_func(field_map, trade):
#     formatted_trade = [f[1](trade[f[0]]) for _, f in field_map.items()]
#     # conditions
#     conds =
#
#     # epoch to datetime
#     self.__convert_timestamp(result, 'sip_timestamp', True)
#     self.__convert_timestamp(result, 'participant_timestamp', True)
#     self.__convert_timestamp(result, 'trf_timestamp', True)
#     # numeric id to MIC  (e.g. 10 -> XNYS)
#     self.__convert_exchange(result, 'exchange')
#     # condition decode
#     conds = []
#     conditions = result.get('conditions')
#     if conditions:
#         for cond_code in conditions:
#             cond_val = self.trade_cond_map.get(cond_code)
#             conds.append(cond_val)
#         result.pop('conditions', None)
#     result['conds'] = conds
#     result['flags'] = self.__convert_conditions_to_flags(conds, result)
