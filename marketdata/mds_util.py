#                                                                               
#  MIT License                                                                  
#                                                                               
#  Copyright (c) 2019 Proof Trading, Inc.                                       
#                                                                               
#  Permission is hereby granted, free of charge, to any person obtaining a copy 
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights 
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
#  copies of the Software, and to permit persons to whom the Software is        
#  furnished to do so, subject to the following conditions:                     
#                                                                               
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.                              
#                                                                               
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.                                                                    
#                                                                               

import json
import logging

from util import datetime_to_millis

LOG = logging.getLogger(__file__)


def get_response_ts_range(resp):
    return resp['start'], resp['end']


def generate_response(sym, start, end, mode, granularity, ticks, nbbo=None):
    start_millis = datetime_to_millis(start)
    end_millis = datetime_to_millis(end)
    LOG.info(f'Generating response, start=[{start}|{start_millis}], end=[{end}|{end_millis}], '
             f'first_tick={ticks[0][0] if ticks else 0}, '
             f'{len(ticks)} ticks, {len(nbbo) if nbbo else 0} nbbo')
    # x[0] should be participant time for ticks, and data ought to be sorted by this time
    ticks.sort(key=lambda x: x[0])
    resp = {'sym': sym,
            'start': start_millis, 'end': end_millis,
            'start_str': str(start), 'end_str': str(end),  # for now, for debug
            'mode': mode, 'type': granularity,
            'ticks': ticks}
    if nbbo is not None:
        nbbo.sort(key=lambda x: x[0])
        resp['nbbo'] = nbbo
    return resp


def filter_ticks(ticks, start_millis, end_millis):
    ticks = [tick for tick in ticks if start_millis <= tick[0] < end_millis]
    return ticks


def merge_responses(*responses):
    if not responses:
        return None

    # load json strings as necessary
    responses = list(responses)
    for i in range(len(responses)):
        resp = responses[i]
        if isinstance(resp, str):
            responses[i] = json.loads(resp)

    if len(responses) == 1:
        return responses[0]

    # sort them first by start millis
    responses = sorted(responses, key=lambda x: x['start'])
    LOG.info('Merging responses: %s', [(r['start'], r['end']) for r in responses])

    resp_first = responses[0]
    resp_last = responses[-1]

    # seed values from first response
    sym = resp_first['sym']
    mode = resp_first['mode']
    granularity = resp_first['type']
    ticks = resp_first['ticks']
    nbbo = resp_first['nbbo'] if 'nbbo' in resp_first else []
    prev_end = resp_first['end']

    # check that the subsequent responses are ok to merge and then merge the data
    for resp in responses[1:]:
        if sym != resp['sym']:
            raise ValueError('cannot merge responses with different symbols')
        if mode != resp['mode']:
            raise ValueError('cannot merge responses with different mode')
        if granularity != resp['type']:
            raise ValueError('cannot merge responses with different granularity')
        start = resp['start']
        end = resp['end']
        if prev_end and start != prev_end:
            raise ValueError(f'cannot merge non-contiguous responses. prev_end={prev_end}, start={start}')
        prev_end = end
        ticks.extend(resp['ticks'])
        nbbo.extend(resp.get('nbbo', []))

    # construct the composite response
    start_millis = resp_first['start']
    start = resp_first['start_str']
    end_millis = resp_last['end']
    end = resp_last['end_str']
    LOG.info(f'Constructing response, start=[{start}|{start_millis}], end=[{end}|{end_millis}], '
             f'first_tick={ticks[0][0] if ticks else 0}, '
             f'{len(ticks)} ticks, {len(nbbo) if nbbo else 0} nbbo')
    resp = {'sym': sym,
            'start': start_millis, 'end': end_millis,
            'start_str': start, 'end_str': end,
            'mode': mode, 'type': granularity,
            'ticks': ticks}
    if nbbo:
        resp['nbbo'] = nbbo
    return resp
