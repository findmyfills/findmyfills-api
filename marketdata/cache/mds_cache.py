#                                                                               
#  MIT License                                                                  
#                                                                               
#  Copyright (c) 2019 Proof Trading, Inc.                                       
#                                                                               
#  Permission is hereby granted, free of charge, to any person obtaining a copy 
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights 
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
#  copies of the Software, and to permit persons to whom the Software is        
#  furnished to do so, subject to the following conditions:                     
#                                                                               
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.                              
#                                                                               
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.                                                                    
#                                                                               

import functools
import json
import logging
import os

from marketdata.cache.abc_mds_cache import AbstractMDSCache, NullMDSCache
from marketdata.mds_util import merge_responses, get_response_ts_range
from util import datetime_to_millis, Singleton, millis_to_datetime

LOG = logging.getLogger(__name__)


@Singleton
class MDSCacheProvider:
    # noinspection PyBroadException
    def __init__(self):
        self.default_impl_name = os.environ.get('DEFAULT_CACHE_IMPL', None)
        self.enabled = False
        self.impls = {}

        try:
            if self.default_impl_name:
                # prime the default cache and save it away
                LOG.info(f'Creating default cache implementation: {self.default_impl_name}')
                self.default_impl = self.get_cache_impl(self.default_impl_name)
                self.enabled = True
                return
        except Exception:
            LOG.warning(f'Error creating cache client for: [{self.default_impl_name}]', exc_info=True)

        self.default_impl = NullMDSCache()
        LOG.warning('MDS Caching is disabled')

    def get_cache_impl(self, impl_name):
        if not impl_name:
            return self.default_impl
        impl = self.impls.get(impl_name)
        if not impl:
            if impl_name == 'bigtable':
                from marketdata.cache.bigtable_cache import BigTableCache
                impl = BigTableCache()
            elif impl_name == 'null':
                impl = NullMDSCache()
            elif impl_name == 'postgres':
                from marketdata.cache.postgres_cache import PostgresCache
                impl = PostgresCache()
            else:
                raise ValueError(f'Unknown mds cache implementation {impl_name}')
            self.impls[impl_name] = impl
        return impl


def mds_cached(func_=None, *, impl_name=None, domain=None, enabled=True):
    def wrapper(func):
        @functools.wraps(func)
        def inner_func(*args, **kwargs):
            use_cache = kwargs.pop('cached', True)
            if enabled and use_cache:
                # noinspection PyBroadException
                try:
                    mds_cache_provider = MDSCacheProvider()
                    if mds_cache_provider.enabled:
                        LOG.info(f'Cached call for {func.__name__}'
                                 f'(args={list(map(str,args[1:4]))}, kwargs={kwargs})')
                        mds_cache = mds_cache_provider.get_cache_impl(impl_name)
                        return get_composite_response(mds_cache, domain, func, args, kwargs)
                except Exception:
                    LOG.exception('Error when working with mds cache')
            else:
                LOG.info(f'{func.__name__} called with MDS caching turned off')

            # if cached=False or getting cached response throws an exception, call the underlying function directly
            return func(*args, **kwargs)

        return inner_func

    if func_ is None:
        return wrapper
    else:
        return wrapper(func_)


def get_composite_response(mds_cache: AbstractMDSCache, domain, func, args, kwargs):
    domain = domain or func.__name__
    _, sym, start, end, *_ = args

    start_millis = datetime_to_millis(start)
    end_millis = datetime_to_millis(end)

    cached_responses = get_cached_response(domain, sym, start_millis, end_millis, mds_cache)
    if not cached_responses:
        pre_range = (start_millis, end_millis)
        caches = {}
        post_range = None
    else:
        pre_range, caches, post_range = cached_responses

    pre_resp = get_non_cached_resp(func, pre_range, *args, **kwargs)
    post_resp = get_non_cached_resp(func, post_range, *args, **kwargs)

    responses = []
    if pre_resp:
        responses.append(pre_resp)
        pre_range = get_response_ts_range(pre_resp)
        pre_resp = json.dumps(pre_resp)
        save_cached_response(domain, sym, pre_range, pre_resp, mds_cache)
    responses.extend(caches.values())
    if post_resp:
        responses.append(post_resp)
        post_range = get_response_ts_range(post_resp)
        post_resp = json.dumps(post_resp)
        save_cached_response(domain, sym, post_range, post_resp, mds_cache)

    return merge_responses(*responses)


def get_cached_response(domain, sym, start_millis, end_millis, mds_cache):
    caches = mds_cache.get_cache(domain, sym, start_millis, end_millis)
    cluster = find_largest_range(list(caches.keys()), start_millis, end_millis)
    if not cluster:
        return None

    # delete the cached responses we couldn't use (because we will re-cache parts of those ranges with new data)
    cache_ranges = caches.keys()
    full_set = set(cache_ranges)
    used_set = set(cluster)
    to_delete = full_set.difference(used_set)
    LOG.info(f'Caches found: {cache_ranges}, useful: {cluster}, will delete: {to_delete}')
    if to_delete:
        mds_cache.del_cache(domain, sym, *[r[0] for r in to_delete])
        for r in to_delete:
            caches.pop(r, None)

    pre_range = (start_millis, cluster[0][0])
    post_range = (cluster[-1][1], end_millis)

    return get_if_valid(pre_range), caches, get_if_valid(post_range)


def save_cached_response(domain, sym, ts_range, data, mds_cache):
    mds_cache.set_cache(domain, sym, ts_range[0], ts_range[1], data)


def get_non_cached_resp(func, ts_range, *args, **kwargs):
    if not ts_range:
        return None

    start = millis_to_datetime(ts_range[0])
    end = millis_to_datetime(ts_range[1])
    args = list(args)
    args[2] = start
    args[3] = end
    args = tuple(args)

    return func(*args, **kwargs)


def find_largest_range(ranges, start_millis, end_millis):
    if not ranges:
        return []

    largest_seen = (0, 0)
    collected_seen = []
    current = largest_seen
    collected_current = collected_seen
    for r in ranges:
        eff_r = (max(r[0], start_millis), min(end_millis, r[1]))
        if is_contiguous(current, eff_r):
            current = merge_ranges(current, eff_r)
            collected_current.append(r)
        else:
            if is_larger(current, largest_seen):
                largest_seen = current
                collected_seen = collected_current
            current = eff_r
            collected_current = [r]
    if is_larger(current, largest_seen):
        collected_seen = collected_current

    return collected_seen


def is_contiguous(r1, r2):
    return r1[1] == r2[0]


def is_larger(r1, r2):
    return (r1[1] - r1[0]) > (r2[1] - r2[0])


def merge_ranges(r1, r2):
    return r1[0], r2[1]


def get_if_valid(r):
    return r if r[0] < r[1] else None


# ########################################
# ############ TESTS #####################
# ########################################

test_ranges = [(1, 2), (2, 5), (5, 7), (7, 10), (12, 18)]

# print(find_largest_range(test_ranges, 7, 20))
