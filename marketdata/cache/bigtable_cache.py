#                                                                               
#  MIT License                                                                  
#                                                                               
#  Copyright (c) 2019 Proof Trading, Inc.                                       
#                                                                               
#  Permission is hereby granted, free of charge, to any person obtaining a copy 
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights 
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell    
#  copies of the Software, and to permit persons to whom the Software is        
#  furnished to do so, subject to the following conditions:                     
#                                                                               
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.                              
#                                                                               
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR   
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,     
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE  
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER       
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.                                                                    
#                                                                               

import datetime
import logging
import os

# noinspection PyPackageRequirements
from google.cloud import bigtable
# noinspection PyPackageRequirements
from google.cloud.bigtable.row_filters import ValueRangeFilter, ColumnQualifierRegexFilter, RowFilterChain, \
    ConditionalRowFilter, PassAllFilter

from marketdata.cache.abc_mds_cache import AbstractMDSCache
from util import measure_time, Singleton, millis_to_midnight_millis, EPOCH


@Singleton
class BigTableCache(AbstractMDSCache):
    """
    The structure of a BigTable table can be described as below:
    1) Tables have rows and columns. Each column belongs to one or more column families.
    2) Intersection of row and column is called a cell, which can have multiple versions identified by timestamps.
    3) Rows have row keys, which can be used to retrieve rows (specifically, ranges can be queried)
    4) Column value based filters can be used to filter the results further

    For caching MDS data, we use the following scheme:
    1) Each type of data is stored in a separate table (bars, full ticks, etc)
    2) Each cached record consists of following information: symbol, start millis, end millis, data (json bytes)
    2) Row key is constructed as [space-filled 12 char symbol]_[zero-filled 13 digit start millis]
    3) For each row, the two columns are:  end millis ('e') and data ('v'). Both columns belong to 'd' column family
    4) To query a record, we look for all rows with row_key between [sym_startofday, sym_endmillis)
    5) We further use the column filter to specify that 'e' must be between [sym_startmillis, sym_endofday]
    6) This query can get us multiple ranges within the requested [start millis, end millis). We find the largest
    contiguous block of ranges, and discard the others (we actually delete the unused cache ranges from bigtable)
    7) After finding the largest chunk of cache, we may still need to get data for a period of time before the cache
    and a period of time after the cache. This data is retrieved from the underlying data provider (e.g. polygon)
    and cached away as distinct ranges
    8) The various chunks of responses (pre-cache range, caches, and post-cache range) are then merged together to
    produce a single response
    """

    def __init__(self):
        self.log = logging.getLogger(__name__)

        client = bigtable.Client(project='findmyfills-api', admin=True)
        self.instance = client.instance('findmyfills')

        self.column_family = 'd'
        self.value_column = en('v')
        self.end_time_column = en('et')

        # prime table cache
        tables = self.instance.list_tables()
        # t.name is of the form 'projects/findmyfills-api/instances/findmyfills/tables/get_full_ticks'
        self.tables = {os.path.basename(t.name): t for t in tables}
        self.log.info(f'Discovered tables: {self.tables.keys()}')

    def get_table(self, table_id):
        if table_id in self.tables:
            return self.tables[table_id]

        self.log.info(f'Checking table {table_id}...')
        t1 = datetime.datetime.now()
        table = self.instance.table(table_id)
        if not table.exists():
            self.log.info(f'Creating table {table_id}...')
            table.create(column_families={self.column_family: None})
            self.log.info(f'Table [{table_id}] created in [{datetime.datetime.now() - t1}].')
        else:
            self.log.info(f'Table {[table_id]} exists... Using existing table')

        self.tables[table_id] = table
        return table

    @measure_time
    def get_cache(self, table, sym, start_millis, end_millis):
        """
        Get cached subset of [start, end) for sym/type
        :param table: string identifying type of data being queried (max 10 chars)
        :param sym: symbol (max 12 chars)
        :param start_millis: start time in millis since epoch
        :param end_millis: end time in millis since epoch
        :return: {(start1, end1): data1, (start2, end2): data2, ...}
            Note that the start/end ranges may be discontiguous and an incomplete subset of the requested start/end.
            Empty list is returned if no cache found.
        """
        table = self.get_table(table)

        midnight = millis_to_midnight_millis(start_millis)
        midnight_next_day = midnight + 86400000
        from_key = get_key(sym, midnight)
        to_key = get_key(sym, end_millis)
        end_time_from = get_end_time(start_millis)
        end_time_to = get_end_time(midnight_next_day)

        self.log.info('Creating filter chain...')
        vrf = ValueRangeFilter(start_value=end_time_from, end_value=end_time_to,
                               inclusive_start=False, inclusive_end=True)
        cqrf = ColumnQualifierRegexFilter(self.end_time_column)
        vchain = RowFilterChain(filters=[vrf, cqrf])
        fchain = ConditionalRowFilter(base_filter=vchain, true_filter=PassAllFilter(True))
        self.log.info(f'Created filters. FromKey={from_key}, ToKey={to_key}, '
                      f'EndTimeFrom={end_time_from}, EndTimeTo={end_time_to}')

        self.log.info('Querying BigTable...')
        partial_rows = table.read_rows(start_key=from_key, end_key=to_key, filter_=fchain)
        self.log.info('Table queried. Reading rows...')
        data = {}
        for row in partial_rows:
            range_start = int(de(row.row_key).split('_')[-1])
            range_end = int(self.extract_cell_value(row, self.end_time_column))
            range_data = self.extract_cell_value(row, self.value_column)
            data[(range_start, range_end)] = range_data
        self.log.info(f'Read {len(data)} rows.')
        return data

    @measure_time
    def set_cache(self, table, sym, start_millis, end_mills, data):
        """
        Saves cached data. Does not check if data already exists.
        :param table: string identifying type of data being queried (max 10 chars)
        :param sym: symbol (max 12 chars)
        :param start_millis: start time in millis since epoch
        :param end_mills: end time in millis since epoch
        :param data: string data
        :return: No return value
        """
        table = self.get_table(table)

        row_key = get_key(sym, start_millis)
        row = table.row(row_key)

        # setting timestamp=EPOCH ensures we only ever have 1 version of this cell
        row.set_cell(self.column_family, self.value_column, data, timestamp=EPOCH)
        row.set_cell(self.column_family, self.end_time_column, get_end_time(end_mills), timestamp=EPOCH)
        row.commit()

    @measure_time
    def del_cache(self, table, sym, *start_times):
        """
        Deletes cached data for provided sym/start_times
        :param table: string identifying type of data being queried (max 10 chars)
        :param sym: symbol (max 12 chars)
        :param start_times: list of start_millis
        :return: No return value
        """
        table = self.get_table(table)

        rows = []
        for start_millis in start_times:
            row_key = get_key(sym, start_millis)
            row = table.row(row_key)
            row.delete()
            rows.append(row)

        table.mutate_rows(rows)

    def extract_cell_value(self, row, column):
        row_data = row.cells[self.column_family][column]
        if row_data:
            row_data = row_data[0]
            if row_data and row_data.value:
                row_data = de(row_data.value)
                return row_data
        return ''


def en(s):
    return s.encode('utf-8')


def de(b):
    return b.decode('utf-8')


def get_key(sym, start_millis):
    return en(f'{sym}_{_format_time(start_millis)}')


def get_end_time(tm_millis):
    return en(_format_time(tm_millis))


def _format_time(tm_millis):
    return f'{tm_millis:0>13}'


# #########################################
# ############# TESTS BELOW ###############
# #########################################


def print_rows(c):
    t = c.get_table('trades')
    for row in t.read_rows():
        c.log.info('[%s]=%s', row.row_key, {k.decode(): v[0].value for k, v in row.to_dict().items()})


def set_cache(c):
    num = 10
    base_ts = 1561988700000
    for i in range(num):
        start = base_ts + i * 10000
        end = start + 500
        print('Setting ', start, end)
        c.set_cache('trades', 'IBKR', start, end, f'mydata-{i}')


def del_cache(c):
    c.del_cache('trades', 'IBKR', 1561988750000, 1561988760000, 1561988770000)


def test_cache(c):
    c.log.info(c.get_cache('trades', 'IBKR', 1561988736000, 15619887774000))


def do_test():
    logging.basicConfig(level='DEBUG')
    c = BigTableCache()
    set_cache(c)
    print_rows(c)
    test_cache(c)
    del_cache(c)
    print_rows(c)
    test_cache(c)

# do_test()
