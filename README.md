# findmyfills-api

This is a Python RESTful API service that:
1. interfaces with findmyfills front end to provide data using a uniform data contract
1. interfaces with polygon.io (or another tick data provider) and retrieves tick data 
    - includes smart caching to avoid spamming the tick data source
1. matches given fills to trades on the tape and performs other fill-level analytics