#
#  MIT License
#
#  Copyright (c) 2019 Proof Trading, Inc.
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#

import concurrent.futures
import datetime
import functools
import logging
import math
import os
import time

import numpy as np
import pandas as pd
from werkzeug.exceptions import BadRequest

LOG = logging.getLogger(__name__)
EPOCH = datetime.datetime.utcfromtimestamp(0)


def chunks(list_to_chunk, size):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(list_to_chunk), size):
        yield list_to_chunk[i:i + size]


def ms_to_datetime64(df, *col_names):
    """Convert dataframe column from ms (float) to datetime64"""
    for cn in col_names:
        df[cn] = pd.to_datetime(df[cn], unit='ms')


def translate_to_datetime64(dicts, *field_names):
    """Convert fields in each trade to a datetime64"""
    for d in dicts:
        for fn in field_names:
            if not isinstance(d[fn], np.datetime64):
                d[fn] = np.datetime64(int(d[fn] * 1000 * 1000), 'ns')


def to_pydatetime(d64):
    """Use convert d64 to python datetime"""
    # return pd.Timestamp(d64).to_pydatetime()
    if d64.dtype.str.endswith('M8[ms]') or d64.dtype.str.endswith('M8[s]'):
        return d64.astype(datetime.datetime)
    else:
        return datetime.datetime.fromtimestamp(d64.astype('int64') / 1e9)


def del_fields(object_list, *field_names):
    for o in object_list:
        for fn in field_names:
            del o[fn]


def get_date_string(dt, format_str):
    """ convert datetime/Timestamp/datetime64 to datetime string (up to micros) """
    date_str = None
    if type(dt) == np.datetime64:
        dt = to_pydatetime(dt)
    date_type = type(dt)
    if date_type == datetime.datetime or date_type == pd.Timestamp:
        date_str = dt.strftime(format_str)
    return date_str


def get_time_in_ms(dt):
    ns = get_time_in_ns(dt)
    if ns is None:
        return None
    return ns / 1000000


def get_time_in_ns(dt):
    """ convert to time in ns """
    ns = None
    if type(dt) == pd.Timestamp:
        dt = dt.to_datetime64()
    if type(dt) == np.datetime64:
        if dt.dtype.str.endswith('M8[ns]'):
            ns = dt.astype('int64')
        elif dt.dtype.str.endswith('M8[ms]'):
            ns = dt.astype('int64') * 1000 * 1000
        elif dt.dtype.str.endswith('M8[us]'):
            ns = dt.astype('int64') * 1000
        elif dt.dtype.str.endswith('M8[s]'):
            ns = dt.astype('int64') * 1000 * 1000 * 1000
        else:
            raise ValueError("Unsupported dtype for datetime64=%s" % dt.dtype.str)
    elif type(dt) == datetime.datetime:
        ns = dt.timestamp() * 1000 * 1000 * 1000
    return ns


def datetime_to_millis(dt):
    return math.floor(dt.timestamp() * 1000)


def millis_to_datetime(ms):
    return datetime.datetime.fromtimestamp(ms / 1000, tz=datetime.timezone.utc)


def millis_to_midnight_millis(ms):
    return int((ms // 86400000) * 86400000)


# Class used to signal BadRequest from within the non-flask classes
class BadInputError(BadRequest):
    pass


# Singleton Class Decorator
# https://python-3-patterns-idioms-test.readthedocs.io/en/latest/Singleton.html
class Singleton:
    def __init__(self, klass):
        self.klass = klass
        self.instance = None

    def __call__(self, *args, **kwargs):
        if self.instance is None:
            self.instance = self.klass(*args, **kwargs)
        return self.instance


class MeasureTime:
    """
    Time measurement class to be used as a context manager. Example:
        with MeasureTime('description'):
            do_something()
    """

    def __init__(self, description, logger=None, log_level=logging.INFO):
        self.description = description
        if not logger:
            logger = logging.getLogger('measure_time')
        self.logger = logger
        self.log_level = log_level

    def __enter__(self):
        self.start = time.time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end = time.time()
        self.logger.log(self.log_level, '%s completed in %ss', self.description, f'{(self.end - self.start):0.3f}')


# Are we having fun yet?
# https://realpython.com/primer-on-python-decorators/#decorators-with-arguments
def measure_time(_fn=None, *, log_level=logging.INFO):
    """
    Example uses:

    @measure_time
    def some_method:
        pass

    @measure_time(log_level='DEBUG')
    def some_method:
        pass
    """

    def measure_time_decorator(fn):
        @functools.wraps(fn)
        def inner_function(*args, **kwargs):
            desc = f'{fn.__name__}(args={args[1:4]}, kwargs={kwargs})'
            logger = None
            if args:
                if hasattr(args[0], 'log'):
                    logger = args[0].log
                elif hasattr(args[0], 'logger'):
                    logger = args[0].logger
            with MeasureTime(desc, logger, log_level):
                return fn(*args, **kwargs)

        return inner_function

    if _fn is None:
        return measure_time_decorator
    else:
        return measure_time_decorator(_fn)


def ceil_dt(dt: datetime.datetime, res):
    res_in_sec = res.total_seconds()
    delta = res_in_sec - dt.timestamp() % res_in_sec
    if delta == res_in_sec:
        delta = 0
    return dt + datetime.timedelta(seconds=delta)


def floor_dt(dt: datetime.datetime, res):
    delta = dt.timestamp() % res.total_seconds()
    return dt - datetime.timedelta(seconds=delta)


def parallel_exec(func_infos, workers=3, raise_errors=False):
    """
    Execute functions in parallel
    (based on https://docs.python.org/3/library/concurrent.futures.html)
    :param func_infos: list of 3 items tuples (id, func, args_array)
    :param workers:
    :param raise_errors: if true, errors will be re-raised when unpacking the future, otherwise err returned in results
    :return: dict of id to results.
    """
    t1 = datetime.datetime.now()

    try:
        results = {}
        with concurrent.futures.ThreadPoolExecutor(max_workers=workers) as executor:
            future_to_ids = {executor.submit(func_info[1], *func_info[2]): func_info[0] for func_info in func_infos}
            for future in concurrent.futures.as_completed(future_to_ids):
                call_id = future_to_ids[future]
                try:
                    data = future.result()
                    results[call_id] = data
                    LOG.debug("%s -> %s", call_id, str(data)[0:100])
                except Exception as exc:
                    LOG.error('call %r generated an exception: %s' % (call_id, exc))
                    if raise_errors:
                        raise exc
                    else:
                        results[call_id] = exc
        return results
    finally:
        LOG.info(f'Parallel exec time [{datetime.datetime.now() - t1}] '
                 f'for [{len(func_infos)}] calls with [{workers}] workers')


def get_gae_service():
    return os.environ.get('GAE_SERVICE', default='UnknownService')


def get_gae_version():
    return os.environ.get('GAE_VERSION', default='UnknownVersion')


def is_dev_service():
    return get_gae_service() == 'dev'
