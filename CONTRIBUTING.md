# Contributing to FindMyFills
We love your input! We want to make contributing to this project as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features

## We Develop with Gitlab
We use gitlab to host code, to track issues and feature requests, as well as accept pull requests.

## Making Contributions

Pull requests are the best way to propose changes to the codebase. We actively welcome your pull requests:

1. Fork the repo and create your branch from `master`.
2. If you've added code that should be tested, add tests.
3. If you've changed APIs, ensure you didn't break any API contracts, or appropriate front-end changes are made [here](https://gitlab.com/findmyfills/findmyfills.gitlab.io).
4. Ensure the tests pass.
5. Make sure your code lints.
6. Issue that pull request!

If you are unable to create Pull requests or test your changes, but believe that you have a valuable change, feel free to mail us a patch file at fmf@prooftrading.com. 

## Report bugs using GitLab [issues](https://gitlab.com/findmyfills/findmyfills-api/issues)
- We use GitLab issues to track public bugs. Report a bug by [opening a new issue](https://gitlab.com/findmyfills/findmyfills-api/issues/new); it's that easy!
- Write bug reports with detail, background, and sample code

## Use a Consistent Coding Style

* For the most part, adopt the coding style of the file you are editing
* Use [pep8](https://pypi.org/project/pep8/) to lint your edits, if necessary

## License
By contributing, you agree that your contributions will be licensed under its MIT License.
