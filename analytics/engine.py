#
#  MIT License
#
#  Copyright (c) 2019 Proof Trading, Inc.
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#

import datetime
import logging
from abc import ABC, abstractmethod
from enum import Enum

import numpy as np
import pandas as pd

from util import to_pydatetime, translate_to_datetime64, get_time_in_ms

PRICE_MULT = 10000


class Side(str, Enum):
    buy = 'BUY'
    sell = 'SELL'


class FillView(ABC):
    """ Helper class to pull fill fields from object (most likely a dict) """

    @abstractmethod
    def get_time(self, fill) -> np.datetime64:
        pass

    @abstractmethod
    def get_symbol(self, fill) -> str:
        pass

    @abstractmethod
    def get_side(self, fill) -> Side:
        pass

    @abstractmethod
    def get_price(self, fill) -> float:
        pass

    @abstractmethod
    def get_size(self, fill) -> int:
        pass

    @abstractmethod
    def get_last_mkt(self, fill) -> str:
        pass


class FMFAnalytics:
    def __init__(self, polygon, fill_view,
                 parallel_asof=False, parallel_workers=16, markout_duration=np.timedelta64(1, 's')):
        self.ds = polygon
        self.fill_view = fill_view
        self.parallel_asof = parallel_asof
        self.parallel_workers = parallel_workers
        self.markout_duration = markout_duration
        self.log = logging.getLogger(__name__)

    def summarize(self, fills, arrival, vwap, end_px, reversion_px):
        """Returns a dict of stats for all fills"""
        """
        FMF stats:
        1. Volume
        2. Number of trades
        3. Average trade size (volume / Num of fills)
        4. Average price (this is a volume weighed avg)
        5. Slippage vs arrival mid (avg price - arrival mid for buy orders, sign flipped for sell orders)
        6. Slippage vs vwap (vwap during the life of the order)
        7. 1-second trade to mid markouts
        8. 3-minute post-order reversion (nbbo midpoint at the end of the order vs. 3 mins later)
        """

        count = len(fills)
        volume = sum([self.fill_view.get_size(f) for f in fills])
        notional = sum([self.fill_view.get_size(f) * int(self.fill_view.get_price(f) * PRICE_MULT) for f in fills])
        avg_size = volume / count
        avg_price = int(notional / volume)

        side = self.fill_view.get_side(fills[0])
        slippage_arrival = FMFAnalytics.__calc_slippage(side, avg_price, arrival)
        slippage_vwap = FMFAnalytics.__calc_slippage(side, avg_price, vwap)
        reversion = FMFAnalytics.__calc_slippage(side, end_px, reversion_px)
        return {
            "volume": volume,
            "count": count,
            "avg_size": avg_size,
            "avg_price": avg_price,
            "arrival": arrival,
            "vwap": vwap,
            "end_midpoint": end_px,
            "reversion_px": reversion_px,
            "slippage_arrival": slippage_arrival,
            "slippage_vwap": slippage_vwap,
            "reversion": reversion,
        }

    def get_fill_markout_time(self, fill):
        return to_pydatetime(self.fill_view.get_time(fill) + self.markout_duration)

    def enrich(self, fills, nbbos):
        """
        Calculates per trade stats and appends to each trade.
        Return list of (fill, markout_data)
        """
        time_field = 'sip_timestamp'
        side = self.fill_view.get_side(fills[0])
        enriched_data = []
        for fill in fills:
            fill_px = int(self.fill_view.get_price(fill) * PRICE_MULT)
            fill_markout_time = self.get_fill_markout_time(fill)
            markout_nbbo = nbbos.get(fill_markout_time)
            if markout_nbbo:
                translate_to_datetime64([markout_nbbo], time_field)
                markout_px = FMFAnalytics.calc_mid(markout_nbbo)
                markout = FMFAnalytics.__calc_markout(side, fill_px, markout_px)
                markout_time = markout_nbbo[time_field]
            else:
                markout_px = None
                markout = None
                markout_time = None
            data = {
                'markout': markout,
                'markout_px': markout_px,
                'markout_time': markout_time
            }
            enriched_data.append((fill, data))
        return enriched_data

    def get_nbbos_asof(self, sym, times):
        if self.parallel_asof:
            return self.ds.get_nbbo_asof_parallel(sym, times, self.parallel_workers)
        else:
            return {time: self.ds.get_nbbo_asof(sym, time, True) for time in times}

    def calculate(self, fills, trades=None, time_field='participant_timestamp', start_time=None):
        """enrich and summarize returns a tuple of two items (summary, enriched_fills) """
        sym = self.fill_view.get_symbol(fills[0])
        start_time = to_pydatetime(self.fill_view.get_time(fills[0])) if start_time is None else start_time
        end_time = to_pydatetime(self.fill_view.get_time(fills[-1]))
        rev_time = end_time + datetime.timedelta(seconds=180)

        # get trades and calc vwap
        # if we have the same start/end times, our fills constitute the VWAP!
        if start_time == end_time:
            fills_dict = [{'price': self.fill_view.get_price(f), 'size': self.fill_view.get_size(f), 'flags': 'v'}
                          for f in fills]
            vwap = self.calc_vwap(fills_dict)
        else:
            if trades is None:
                trades = self.ds.get_trades_v2(sym, start_time, end_time, self.ds.NO_QUERY_LIMIT, True)
            else:
                # filter provided trades for start/end times
                start_millis = get_time_in_ms(start_time)
                end_millis = get_time_in_ms(end_time)
                trades = [t for t in trades if start_millis <= t[time_field] <= end_millis]
                # TODO: filter trades by order limit px for in-limit VWAP somehow
            vwap = self.calc_vwap(trades)

        # nbbo of first fill, end of order and 3 min reversion and per fill 1s markouts
        markout_times = [self.get_fill_markout_time(fill) for fill in fills]
        nbbos = self.get_nbbos_asof(sym, [start_time, end_time, rev_time] + markout_times)
        nbbo_arrival = nbbos.get(start_time)
        nbbo_end = nbbos.get(end_time)
        nbbo_3min_rev = nbbos.get(rev_time)

        arrival_px = FMFAnalytics.calc_mid(nbbo_arrival)
        end_px = FMFAnalytics.calc_mid(nbbo_end)
        rev_px = FMFAnalytics.calc_mid(nbbo_3min_rev)

        fill_summary = self.summarize(fills, arrival_px, vwap, end_px, rev_px)
        enriched_fills = self.enrich(fills, nbbos)

        mn = 0
        vol = 0
        for fill, data in enriched_fills:
            markout = data['markout']
            size = self.fill_view.get_size(fill)
            if markout is not None:
                mn += markout * self.fill_view.get_size(fill)
                vol += size
        fill_summary['avg_markout'] = int(mn / vol) if vol > 0 else None
        return fill_summary, enriched_fills

    @staticmethod
    def calc_mid(nbbo):
        if not nbbo:
            return None
        bid = nbbo.get('bid_price')
        bid = int(bid * PRICE_MULT) if bid else None
        ask = nbbo.get('ask_price')
        ask = int(ask * PRICE_MULT) if ask else None

        if bid and ask:
            spread = abs(ask - bid)
            mid = (bid + ask) / 2
            return None if spread > 0.2 * mid else int(mid)
        elif bid and not ask:
            return bid
        elif ask and not bid:
            return ask
        else:
            return None

    @staticmethod
    def __calc_slippage(side, px, benchmark):
        if benchmark is None or px is None:
            return None
        if side == Side.buy:
            return px - benchmark
        else:
            return benchmark - px

    @staticmethod
    def __calc_markout(side, px, markout):
        if markout is None or px is None:
            return None
        if side == Side.buy:
            return markout - px
        else:
            return px - markout

    @staticmethod
    def calc_vwap(trades) -> float:
        """ VWAP of all volume eligible trades in the time range"""
        notional = 0
        volume = 0
        for t in trades:
            if 'v' in t.get("flags", ""):
                px = int(t.get("price") * PRICE_MULT)
                sz = t.get("size")
                notional += px * sz
                volume += sz

        return int(notional / volume) if volume > 0 else None

    def tape_match(self, fills, trades=None, buffer_ms=1000, time_field='participant_timestamp'):
        """
        Basic algorithm for matching fills to the tape

        1. Look at each fill individually
        2. Take the time stamp +/- one second (aka the buffer)
        3. If there are any fills in that window with the same price and size, it's a match! Otherwise it's unmatched.
        :param fills:
        :param trades: [optional] will query if None are sent
        :param buffer_ms:
        :param time_field:
        :return:
        """
        # get trades over time period +/- buffer
        if trades is None:
            trades = self.get_possible_trades_for_tape_match(fills, buffer_ms)

        # create data frame to query trades from
        df_trades = pd.DataFrame(trades)

        # process each fill, trying fills with last market first since they could match exactly, narrowing results
        prev_match_state = {}
        fill_index_to_matches = {}
        for i in range(0, len(fills)):
            if self.ds.validate_venue(self.fill_view.get_last_mkt(fills[i])):
                fill_index_to_matches[i] = self.find_trades_for_fill(fills[i], trades, df_trades, buffer_ms, time_field,
                                                                     prev_match_state)
        for i in range(0, len(fills)):
            if i not in fill_index_to_matches:
                fill_index_to_matches[i] = self.find_trades_for_fill(fills[i], trades, df_trades, buffer_ms, time_field,
                                                                     prev_match_state)
        enriched_fills = [(fills[i], fill_index_to_matches[i]) for i in range(0, len(fills))]

        # summarize
        summary = self.summarize_matches(enriched_fills)

        return summary, enriched_fills

    def get_possible_trades_for_tape_match(self, fills, buffer_ms=1000):
        first_fill = fills[0]
        last_fill = fills[-1]
        sym = self.fill_view.get_symbol(first_fill)
        buffer_delta = datetime.timedelta(milliseconds=buffer_ms)
        start_time = to_pydatetime(self.fill_view.get_time(first_fill)) - buffer_delta
        end_time = to_pydatetime(self.fill_view.get_time(last_fill)) + buffer_delta
        trades = self.ds.get_trades_v2(sym, start_time, end_time, self.ds.NO_QUERY_LIMIT, True)
        return trades

    def find_trades_for_fill(self, fill, trades, df_trades_all, buffer_ms, time_field, prev_match_state=None):
        """
        1) select trades by fill_time (+/- buffer), size, price
        2) pull out trades

        Questions/Enhancement:
        - if we have an exact match, should we remove it from the possible next matches?

        :param fill:
        :param trades: all possible trades (non-data-frame)
        :param df_trades_all: data frame of all possible trades
        :param buffer_ms: time buffer in ms
        :param time_field: field in dataframe with time
        :param prev_match_state: set of indexes in the data frame that have matches
        :return: list of possible matches
        """
        if prev_match_state is None:
            prev_match_state = {}
        self.log.debug('find_trades_for_fill(%s, trades=%d, buffer=%dms, tf=%s, prev=%s)' %
                       (fill, len(df_trades_all), buffer_ms, time_field, prev_match_state))

        # no trades, no matches
        if len(trades) == 0:
            return []

        # disable warning for creating new column on a dataframe slice
        pd.options.mode.chained_assignment = None

        fill_size = self.fill_view.get_size(fill)
        fill_price = self.fill_view.get_price(fill)
        fill_last_mkt = self.ds.validate_venue(self.fill_view.get_last_mkt(fill))
        fill_time = self.fill_view.get_time(fill)

        # select possible trades in time buffer
        fill_time_ms = to_pydatetime(fill_time).timestamp() * 1000
        start_time_ms = fill_time_ms - buffer_ms
        end_time_ms = fill_time_ms + buffer_ms
        time_series = df_trades_all[time_field]
        matched_labels = [k for k, v in prev_match_state.items() if v == 'exact']
        df_trades = df_trades_all[
            (time_series >= start_time_ms) & (time_series <= end_time_ms) &
            (df_trades_all['size'] == fill_size) & np.isclose(df_trades_all['price'], fill_price) &
            (~df_trades_all.index.isin(matched_labels))
            ]

        if len(df_trades) > 1 and fill_last_mkt is not None:
            # tie break on valid last mkt
            df_trades_venue = df_trades[df_trades['exchange'] == fill_last_mkt]
            if len(df_trades_venue) > 0:
                df_trades = df_trades_venue
            # else:
            #     # tie break on time?
            #     df_trades['time_diff'] = df_trades[time_field] - fill_time_ms

        match_count = len(df_trades)
        if match_count == 1:
            # mark exact match to prevent it from being found again
            key = df_trades.index[0]
            prev_match_state[key] = 'exact'
            self.log.debug('saving exact match for label [%s]', key)
        elif match_count > 1:
            # multi-match - sort results by multi-match count
            df_trades['prev_partial_match'] = df_trades.index.map(prev_match_state).fillna(0)
            df_trades.sort_values('prev_partial_match', inplace=True, kind='mergesort')

            # mark first trade in multi-match so it is lower in rank in the future: N.B. - we return the first match
            key = df_trades.index[0]
            m = prev_match_state.get(key, 0)
            prev_match_state[key] = m + 1

        # extract each match
        if self.log.isEnabledFor(logging.DEBUG):
            self.log.debug('matches for %s\n%s', fill, df_trades)
            self.log.debug('prev_match_state=%s', prev_match_state)
        matches = []
        for row in df_trades.itertuples():
            matches.append(trades[row.Index])

        return matches

    @staticmethod
    def summarize_matches(enriched_fills):
        matched_fills = 0
        matched_trades = 0
        venue_stats = {}
        for fill, matches in enriched_fills:
            m = len(matches)
            if len(matches) > 0:
                matched_fills += 1
            for match in matches:
                exch = match['exchange']
                n = venue_stats.get(exch, 0)
                venue_stats[exch] = n + 1
            matched_trades += m
        return {
            "matched_fills": matched_fills,
            "matched_trades": matched_trades,
            "venue_stats": venue_stats
        }
