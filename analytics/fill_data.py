#
#  MIT License
#
#  Copyright (c) 2019 Proof Trading, Inc.
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#

import datetime
import json
import logging

import dateutil
import numpy as np
import pandas as pd

import util
from analytics.engine import FillView, Side, FMFAnalytics
from util import BadInputError, get_time_in_ms

LOG = logging.getLogger(__name__)

known_headers = {
    'TIME': {'required': True, 'apply': pd.to_datetime, 'apply_kwargs': {'infer_datetime_format': True}},
    'SYMBOL': {'required': True, 'apply': str},
    'SIDE': {'apply': str},
    'PRICE': {'required': True, 'apply': pd.to_numeric},
    'SIZE': {'required': True, 'apply': pd.to_numeric},
    'VENUE': {'apply': str}
}


class FillDataParser:
    def __init__(self, sym, side, date, tz, data):
        self.sym = sym
        self.side = side
        self.date = date
        self.tz = tz
        self.data = data

        LOG.info(f'FillDataParser created for sym={sym}, date={self.date}, tz={tz}, fills={len(self.data)}')

    def process(self):
        df = pd.DataFrame(self.data)

        # promote first row as header
        headers = df.iloc[0]
        LOG.info(f'Discovered headers: {list(headers)}')
        df = df[1:]
        df.columns = headers
        df.reset_index(inplace=True, drop=True)

        # determine if the included timestamps are time-only
        date_fixup_required = is_timeonly_format(df, 'TIME')

        # apply column definitions
        for col, cdef in known_headers.items():
            if col not in df.columns:
                if cdef.get('required', False):
                    raise BadInputError(f'Required column [{col}] not found')
                continue
            df[col] = df[col].apply(cdef['apply'], **cdef.get('apply_kwargs', {}))

        # if time in ET, convert to UTC
        if self.tz == 'ET':
            df['TIME'] = df['TIME'].dt.tz_localize('US/Eastern') \
                .dt.tz_convert('UTC') \
                .dt.tz_localize(None)
        df['TIME'] = df['TIME'].apply(lambda x: x.to_datetime64())

        if 'SIDE' not in df.columns:
            df['SIDE'] = self.side

        # check that we have some records
        if len(df.index) == 0:
            raise BadInputError('No fills could be read')

        # infer symbol and date if not specified
        if not self.sym:
            self.sym = df['SYMBOL'].iat[0]
        if not self.side:
            self.side = df['SIDE'].iat[0]
        if not self.date:
            self.date = df['TIME'].iat[0]

        # format the parameters
        self.side = convert_to_side(self.side)
        self.date = pd.to_datetime(self.date, infer_datetime_format=True)

        # fixup timestamps to specified date if only time strings were used
        if date_fixup_required:
            adjust_timestamps_to_date(df, 'TIME', self.date)

        LOG.info(f'Fills loaded: {len(df.index)}')
        # filter rows for symbol and side
        df['SIDE'] = df['SIDE'].apply(lambda x: convert_to_side(x if x else self.side))
        df = df.loc[(df['SYMBOL'] == self.sym) & (df['SIDE'] == self.side)]
        LOG.info(f'Fills with requested symbol={self.sym}, side={self.side}: {len(df.index)}')
        # filter rows by date
        start_date = self.date
        end_date = start_date + pd.Timedelta(days=1)
        df = df.loc[df['TIME'].between(start_date, end_date)]
        LOG.info(f'Fills after filtering for date={start_date}: {len(df.index)}')

        # sort df by time. FMFAnalytics requires this.
        df.sort_values(by=['TIME'], inplace=True)

        if df.empty:
            raise BadInputError('No fills could be read for requested symbol and date')

        return df


def construct_response(summary, fills):
    return {'summary': summary, 'fills': fills}


class FillDataAnalyzer:
    def __init__(self, data_frame, data_source):
        self.df = data_frame
        self.ds = data_source
        self.fmf_analytics = FMFAnalytics(self.ds, DictFillView(), parallel_asof=True)

    def analyze(self):
        fills = self.df.to_dict('records')
        if not fills:
            LOG.info(f'No fills detected, returning empty result')
            return construct_response({}, [])

        fill_count = len(fills)

        LOG.info(f'Attempting to fetch possible trades for {fill_count} fills...')
        t1 = datetime.datetime.now()
        trades = self.fmf_analytics.get_possible_trades_for_tape_match(fills)
        LOG.info(f'Retrieved {len(trades)} trades in {datetime.datetime.now() - t1}')

        LOG.info(f'Attempting to tape-match {fill_count} fills...')
        t1 = datetime.datetime.now()
        _, tape_matched_fills = self.fmf_analytics.tape_match(fills, trades)
        matched_fills = []
        found_matches = 0
        for fill, matches in tape_matched_fills:
            fill['matched'] = len(matches)
            if matches:
                found_matches += 1
                match = matches[0]
                fill['TIME'] = pd.to_datetime(match['participant_timestamp'], unit='ms')
                fill['VENUE'] = match['exchange']
                LOG.debug(f'Matched fill: {fill}')
                LOG.debug(f'Full matched fill: {match}')
            else:
                fill['VENUE'] = 'N/A'
            matched_fills.append(fill)
        LOG.info(f'Tape-matching completed, found {found_matches}/{fill_count} in {datetime.datetime.now() - t1}')

        LOG.info(f'Analyzing {fill_count} fills...')
        t1 = datetime.datetime.now()
        summary, fill_data = self.fmf_analytics.calculate(fills, trades)
        LOG.info(f'Analysis completed in {datetime.datetime.now() - t1}')
        LOG.info(f'Summary: {summary}')
        enriched_fills = []
        for fill, data in fill_data:
            fill['TIME'] = get_time_in_ms(fill['TIME'])
            data['markout_time'] = get_time_in_ms(data['markout_time'])
            fill.update(data)
            enriched_fills.append(fill)

        return construct_response(summary, enriched_fills)


class DictFillView(FillView):
    def get_time(self, fill) -> np.datetime64:
        return self.get_column_value(fill, 'TIME').to_datetime64()

    def get_symbol(self, fill) -> str:
        return self.get_column_value(fill, 'SYMBOL')

    def get_side(self, fill) -> Side:
        return self.get_column_value(fill, 'SIDE')

    def get_price(self, fill) -> float:
        return self.get_column_value(fill, 'PRICE')

    def get_size(self, fill) -> int:
        return self.get_column_value(fill, 'SIZE')

    def get_last_mkt(self, fill) -> str:
        return self.get_column_value(fill, 'VENUE')

    @staticmethod
    def get_column_value(fill, column):
        return fill[column]


def convert_to_side(side_str):
    return Side.buy if side_str and side_str.lower().startswith('b') else Side.sell


def is_timeonly_format(df, time_col):
    time_col = df[time_col]
    if len(time_col) > 0:
        val_to_test = time_col.iat[0]
        # noinspection PyBroadException
        try:
            parsed = dateutil.parser.parse(val_to_test, default=util.EPOCH)
            if parsed.year == util.EPOCH.year:
                return True
        except Exception:
            # best efforts
            pass
    return False


def adjust_timestamps_to_date(df, time_col, target_date):
    # this is the 'default' date used by dateutil.parser.parse if only a time string is specified
    today = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    # fix up times by subtracting the default date and add the target_date to all values
    df[time_col] = df[time_col].apply(lambda dt: dt - today + target_date)


################################
# ######### TESTS ##############
################################


sample_data = [['TIME', 'SYMBOL', 'SIDE', 'PRICE', 'SIZE', 'VENUE'],
               ['2019-07-01 9:53:12', 'IBM', 'buy', '17.23', '98', 'NYSE'],
               ['2019-06-13 11:34:46.280', 'UBNT', 'sell', '132.11', '75', ''],
               ['2019-06-13 11:00:25.658', 'UBNT', 'buy', '132.59', '23', ''],
               ['2019-07-02 11:53:12.4322334433', 'IBM', 'ss', '.23', '1322', 'CBOE']]

sample_data2 = [['TIME', 'SYMBOL', 'SIDE', 'PRICE', 'SIZE', 'VENUE'],
                ['11:34:46.280', 'UBNT', 'sell', '132.11', '75', ''],
                ['11:00:25.658', 'UBNT', 'buy', '132.59', '23', '']]


def do_tests():
    import logging_helper
    logging_helper.init_logging('DEBUG')
    f = FillDataParser('UBNT', 'SELL', '2019-06-13', 'ET', sample_data2)
    # f = FillDataParser('UBNT', 'SELL', '2019-06-13', 'ET', sample_data)
    df1 = f.process()
    print(df1)

    from polygon import PolygonDataSource
    fda = FillDataAnalyzer(df1, PolygonDataSource())
    print(json.dumps(fda.analyze(), indent=4))

# from util import MeasureTime
# with MeasureTime('tests'):
#     do_tests()
