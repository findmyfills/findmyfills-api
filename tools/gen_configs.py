#
#  MIT License
#
#  Copyright (c) 2019 Proof Trading, Inc.
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#
import json
import os

# noinspection PyPackageRequirements
import yaml
# noinspection PyPackageRequirements
from jinja2 import Environment, FileSystemLoader

script_dir = os.path.dirname(__file__)
config_dir = os.path.normpath(os.path.join(script_dir, '../secrets'))
output_dir = os.path.normpath(os.path.join(script_dir, '..'))
template_dir = script_dir


def gen_config_files():
    template_file_name = 'template_app.yaml'
    print(f'Loading template {template_file_name} from {template_dir}...')
    # load template
    j_env = Environment(loader=FileSystemLoader(template_dir), trim_blocks=True, lstrip_blocks=True)
    template = j_env.get_template(template_file_name)

    for env in ['dev', 'prod']:
        gen_config_file(env, template)


def gen_config_file(env, template):
    config_data_file = os.path.join(config_dir, f'{env}_configs.yaml')
    output_file = os.path.join(output_dir, f'app_{env}.yaml')
    print(f'Generating {output_file} from {config_data_file}...')

    # load config data
    with open(config_data_file, 'r') as f:
        config_data = yaml.safe_load(f)

    # create strings from lists
    config_data = {k: format_list(v) for k, v in config_data.items()}

    with open(output_file, 'w') as f:
        f.write(template.render(config_data))

    print(f'Generated {output_file}')


def format_list(v):
    if isinstance(v, list):
        v = json.dumps(v)
        # in YAML, two single-quotes escape a single-quote in a single-quoted string
        v = v.replace("'", "''")
        v = f"'{v}'"
    return v


if __name__ == '__main__':
    gen_config_files()
