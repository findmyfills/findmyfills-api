#
#  MIT License
#
#  Copyright (c) 2019 Proof Trading, Inc.
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.
#

import os
import sys
from subprocess import Popen, PIPE

GCLOUD_EXE = r"C:\dev\tools\Google\Cloud SDK\google-cloud-sdk\bin\gcloud.cmd"
CURRENT_SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
DEPLOY_FILE_DIR = os.path.normpath(os.path.join(CURRENT_SCRIPT_DIR, '..'))
GEN_CONFIGS_SCRIPT = os.path.normpath(os.path.join(CURRENT_SCRIPT_DIR, 'gen_configs.py'))
PYTHON_EXE = sys.executable


def deploy(env, version):
    deploy_file = os.path.join(DEPLOY_FILE_DIR, f'app_{env}.yaml')

    print(f'Deploying service={env}, version={version} using {deploy_file}...')
    command = [GCLOUD_EXE, 'app', 'deploy', deploy_file, '-v', version]
    command_input = b'y'

    ret_code = run_command(command, command_input=command_input)
    if ret_code != 0:
        print('Deployment FAILED!')
        exit(1)
    else:
        print('Deployment successful')


def gen_configs():
    print('Generating configs...')
    ret_code = run_command([PYTHON_EXE, GEN_CONFIGS_SCRIPT])
    if ret_code != 0:
        print('Generation of configs FAILED!')
        exit(2)
    else:
        print('Config generation successful')


def run_command(command, shell=True, command_input=None):
    print(f'==== Begin command: {command} ====')
    deploy_proc = Popen(command, shell=shell, close_fds=True, stdin=PIPE)
    deploy_proc.communicate(input=command_input)
    ret_code = deploy_proc.returncode
    print(f'Command return code: [{ret_code}]')
    print(f'==== End command: {command} ====')
    return ret_code


def process():
    argc = len(sys.argv)
    if argc < 2:
        print('Usage: deploy.py <env> [<version>]')
        exit(-1)

    env = sys.argv[1]
    if env not in ['dev', 'prod']:
        print(f'Unexpected environment: {env}')
        exit(-2)

    version = 'v1'
    if argc > 2:
        version = sys.argv[2]

    gen_configs()
    deploy(env, version)


if __name__ == "__main__":
    process()
